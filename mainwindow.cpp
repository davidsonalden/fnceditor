/* Copyright 2017, Alden Davidson (adavidson@protonmail.ch) */

/* This file is part of fncEditor.
 *
 * fncEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fncEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fncEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include <QBoxLayout>
#include <QFileDialog>
#include <QStandardPaths>
#include <QDebug>
#include <QTabBar>
#include <QMessageBox>
#include <QShortcut>
#include <QDesktopWidget>

//define static variables
QTabWidget* MainWindow::mainTabs;

/*
 * constructor
 */
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    //configure main window settings
    resize(QDesktopWidget().availableGeometry(this).size() * 0.65);
    setWindowTitle("fncEditor");

    //initialize and position tabbed text editor
    initTabWidget();
    setCentralWidget(mainTabs);

    //initialize menubar
    initMenuBar();
}

/*
 * Deconstructor
 */
MainWindow::~MainWindow() {
    this->close();
}

/*
 * Initialize the menu bar
 */
void MainWindow::initMenuBar() {
    initFileActions();
    initViewMenu();
    initHelpMenu();
}

/*
 * initiliaze the actions for the File menu tab
 */
void MainWindow::initFileActions() {
    //init file dropdown and add to window
    QMenu* fileMenu = new QMenu(tr("&File"), this);
    menuBar()->addMenu(fileMenu);

    //add entries to dropdown
    fileMenu->addAction(tr("New Tab"), this, SLOT(newTab()), QKeySequence::AddTab);
    fileMenu->addAction(tr("Open"), this, SLOT(openFile()), QKeySequence::Open);
    fileMenu->addSeparator();
    fileMenu->addAction(tr("Save"), this, SLOT(saveFile()), QKeySequence::Save);
    fileMenu->addAction(tr("Save as"), this, SLOT(saveFileAs()), QKeySequence::SaveAs);
    fileMenu->addSeparator();
    fileMenu->addAction(tr("Close tab"), this, SLOT(closeTab()), QKeySequence::Close);
    fileMenu->addAction(tr("Quit"), this, SLOT(quitMain()), QKeySequence::Quit);
}

/*
 * Initialize the actions for the View menu tab
 */
void MainWindow::initViewMenu() {
    //init view dropdown and add to window
    QMenu *viewMenu = new QMenu(tr("&View"), this);
    menuBar()->addMenu(viewMenu);

    //add entries to dropdown
    viewMenu->addAction(tr("Center on scroll"), this, SLOT(toggleCenterOnScoll()));
    viewMenu->addAction(tr("Word Wrap"), this, SLOT(toggleWordWrap()));

    //modify entries
    viewMenu->actions().at(0)->setCheckable(true);
    viewMenu->actions().at(1)->setCheckable(true);
}

/*
 * Initialize the actions for the Help Menu tab
 */
void MainWindow::initHelpMenu() {
    //init help dropdown and add to window
    QMenu *helpMenu = new QMenu(tr("&Help"), this);
    menuBar()->addMenu(helpMenu);

    //add entries to dropdown
    helpMenu->addAction(tr("&About"), this, SLOT(about()));
}

/*
 * Initialize the tabbed text editor
 */
void MainWindow::initTabWidget() {
    //initialize tab filename vector
    openFiles.clear();

    //create tab widget
    mainTabs = new QTabWidget(this);

    //set default settings
    mainTabs->setMovable(true);

    // create initial tab
    newTab();

    //define shortcuts
    initTabShortcuts();

    //connect tab movement monitor
    connect(mainTabs->tabBar(), SIGNAL(tabMoved(int,int)), SLOT(tabOrderModified(int,int)));

}

/*
 * Wrapper functions to allow mapping as slots
 */
//void saveFil

/*
 * initialize shortcuts for tabs
 */
void MainWindow::initTabShortcuts() {
    //definitions
    QShortcut* alt1Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_1), this);
    alt1Keybind->setObjectName("ALT+1");
    QShortcut* alt2Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_2), this);
    alt2Keybind->setObjectName("ALT+2");
    QShortcut* alt3Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_3), this);
    alt3Keybind->setObjectName("ALT+3");
    QShortcut* alt4Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_4), this);
    alt4Keybind->setObjectName("ALT+4");
    QShortcut* alt5Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_5), this);
    alt5Keybind->setObjectName("ALT+5");
    QShortcut* alt6Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_6), this);
    alt6Keybind->setObjectName("ALT+6");
    QShortcut* alt7Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_7), this);
    alt7Keybind->setObjectName("ALT+7");
    QShortcut* alt8Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_8), this);
    alt8Keybind->setObjectName("ALT+8");
    QShortcut* alt9Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_9), this);
    alt9Keybind->setObjectName("ALT+9");
    QShortcut* alt0Keybind = new QShortcut(QKeySequence(Qt::ALT + Qt::Key_0), this);
    alt0Keybind->setObjectName("ALT+0");

    //connections
    connect(alt1Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt2Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt3Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt4Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt5Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt6Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt7Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt8Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt9Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
    connect(alt0Keybind, SIGNAL(activated()), SLOT(changeFocusedTab()));
}

/*
 * Open a new, untitled tab
 */
void MainWindow::newTab() {
    //calculate index left of current tab
    int newTabIndex = this->mainTabs->currentIndex() + 1;
    // create new tab
    QWidget *initTab = new QWidget;
    //create layout for positioning text editor
    QHBoxLayout *initTabEditorLayout = new QHBoxLayout(initTab);
    //insert text editor into new tab
    CodeEditor *initTabEditor = new CodeEditor(initTab);
    initTabEditorLayout->addWidget(initTabEditor);
    //add new tab to tab widget
    mainTabs->insertTab(newTabIndex, initTab, tr("Untitled"));

    //insert placeholder into openFiles
    QFileInfo fi("");
    openFiles.insert(newTabIndex, fi);

    //set text editor settings
    initTextEditor(initTabEditor);

    //connect to textChange monitor
    connect(initTab->findChild<CodeEditor*>(), SIGNAL(textChanged()), SLOT(tabContentsModified()));
}

/*
 * Initialize settings for text editor
 */
void MainWindow::initTextEditor(CodeEditor* editor) {
    editor->setWordWrapMode(QTextOption::NoWrap);

    //Syntax Highlighting Settings
    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(10);

    editor->setFont(font);

    highlighter = new fanucHighlighter(editor->document());
}

/*
 * Open an existing file for editing
 */
void MainWindow::openFile() {
    // get filename
    QString filepath = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    QStandardPaths::writableLocation(QStandardPaths::HomeLocation),
                                                    tr("ASCII Programs (*.ls)"));

    //open file, checking for failure to open
    QFile inputFile(filepath);
    if(inputFile.open(QIODevice::ReadOnly)) {
        // strip filename from path
        QFileInfo fi(filepath);
        QString tmpfilename = fi.fileName();

        //store filename at index of current tab
        openFiles.insert(mainTabs->currentIndex(), fi);

        // get the text editor widget
        CodeEditor* currentTextEditor = mainTabs->currentWidget()->findChild<CodeEditor*>();

        // read all text from file into text editor
        currentTextEditor->setPlainText(inputFile.readAll());

        // close file
        inputFile.close();

        //set tab title to new filename
        mainTabs->setTabText(mainTabs->currentIndex(), tmpfilename);
    } else {
        qWarning() << "Failed to read file";
        return;
    }
}

/*
 * Save file opened in current tab.
 * Defaults to index of current tab
 *
 * Returns(int): function error value
 */
int MainWindow::saveFile(const int& index) {
    //set to -1 to assume error
    int funcError = -1;

    //get fileinfo from master vector, check if exists
    QFileInfo fileinfo = openFiles.at(index);
    if(!fileinfo.exists()) {
        qDebug() << "File doesn't yet exist - calling actionSave_as instead";
        funcError = MainWindow::saveFileAs();
    } else {
        //get filename from fileinfo
        QString filepath = fileinfo.filePath();

        //open file, checking for failure
        QFile outputfile(filepath);
        if(outputfile.open(QIODevice::WriteOnly)) {
            //write to file
            QTextStream out(&outputfile);
            CodeEditor* curText = mainTabs->widget(index)->findChild<CodeEditor*>();
            out << curText->toPlainText();

            //flush and close file
            outputfile.flush();
            outputfile.close();

            //update tab title, signal changes are saved;
            QString curTitle = mainTabs->tabText(index);
            if(curTitle.endsWith("*")) {
                curTitle.chop(1);
                mainTabs->setTabText(index, curTitle);
            }

            //set to non-error
            funcError = 0;
        } else {
            qDebug() << "File couldn't be opened - calling actionSave_as instead";
            funcError = MainWindow::saveFileAs();
        }
    }

    return funcError;
}

/*
 * Save file under new filename.
 * Defaults to index of current tab
 *
 * Returns(int): function error value
 */
int MainWindow::saveFileAs(const int& index) {
    //set to -1 to assume error
    int funcError = -1;

    // get filepath
    QString filepath = QFileDialog::getSaveFileName(this, tr("Open File"),
                                                    QStandardPaths::writableLocation(QStandardPaths::HomeLocation),
                                                    tr("ASCII Programs (*.ls)"));

    //open file, checking for failure to open
    QFile outputfile(filepath);
    if(outputfile.open(QIODevice::WriteOnly)) {
        //store new filepath
        QFileInfo fi(filepath);
        openFiles.insert(index, fi);

        //write to file
        QTextStream out(&outputfile);
        CodeEditor* curText = mainTabs->widget(index)->findChild<CodeEditor*>();
        out << curText->toPlainText();

        //flush and close file
        outputfile.flush();
        outputfile.close();

        //reset tab title to new name
        mainTabs->setTabText(index, fi.fileName());

        //set to non-error
        funcError = 0;
    } else {
        qWarning() << "Failed to read file";
        funcError = -1;
    }

    return funcError;
}

/*
 * Close the currently active tab
 */
void MainWindow::closeTab() {
    //-1 to assume error
    int funcError = -1;

    //only attempt to close tab if tabs exist
    if(mainTabs->currentIndex() >= 0) {
        QString title = mainTabs->tabText(mainTabs->currentIndex());
        if(title.endsWith("*")) {
            funcError = alertUnsavedChanges("tabClose");
        } else {
            funcError = 0;
        }

        //Don't close tab if contents weren't saved
        if(funcError == 0) {
            fullyCloseTab(mainTabs->currentIndex());
        } else {
            qDebug() << "File wasn't saved for some reason";
        }
    }
}

/*
 * Helper function to completely remove all information related to a tab.
 * Ensures there is no leftover info to corrupt runtime when tabs are closed.
 */
void MainWindow::fullyCloseTab(int index) {
    openFiles.remove(index);
    mainTabs->removeTab(index);
}

/*
 * Quit the application
 */
void MainWindow::quitMain() {
    //-1 to assume error
    int funcError = -1;

    //loop through tabs and check for any unsaved data
    for(int tabIndex = 0; tabIndex < mainTabs->count(); ++tabIndex) {
        if(mainTabs->tabText(tabIndex).endsWith("*")) {
            funcError = alertUnsavedChanges("appClose");

            //don't close application if any fail to save
            if(funcError == -1) {
                return;
            } else if(funcError == 1) {
                //all tabs discarded already
                break;
            }
        }
    }
    this->close();
}

/*
 * Toggles the centerOnScroll value
 */
void MainWindow::toggleCenterOnScoll() {
    for(int i = 0; i < mainTabs->count(); ++i) {
        CodeEditor* curTab = mainTabs->widget(i)->findChild<CodeEditor*>();
        curTab->setCenterOnScroll(!(curTab->centerOnScroll()));
    }
}

/*
 * Toggles the wordWrapMode value
 */
void MainWindow::toggleWordWrap() {
    for(int i = 0; i < mainTabs->count(); ++i) {
        CodeEditor* curTab = mainTabs->widget(i)->findChild<CodeEditor*>();
        QTextOption::WrapMode curWrapPolicy = curTab->wordWrapMode();
        if(curWrapPolicy != QTextOption::NoWrap) {
            curTab->setWordWrapMode(QTextOption::NoWrap);
        } else {
            curTab->setWordWrapMode(QTextOption::WordWrap);
        }
    }
}

/*
 * Sets the filename on current tab
 */
void MainWindow::tabContentsModified() {
    int index = mainTabs->currentIndex();
    QString title = mainTabs->tabText(index);
    if(!title.endsWith("*")) {
        title.append("*");
    }
    mainTabs->setTabText(index, title);
}

/*
 * Update openFiles vector whenever a tab is moved to reflect the new positions
 * of all tabs
 */
void MainWindow::tabOrderModified(int from, int to) {
    QFileInfo movedInfo = openFiles.takeAt(from);
    openFiles.insert(to, movedInfo);
}

/*
 * wrapper function to change the current tab of focus
 */
void MainWindow::changeFocusedTab() {
    QString shortcut = QObject::sender()->objectName();
    int newIndex = -1;

    //iterate through all 10 digits that can be pressed with ALT
    for(int i = 0; i < 10; ++i) {
        QString compareStr = QString("ALT+%1").arg(i);
        if(shortcut == compareStr) {
            if(i == 0) {
                newIndex = 9;
            } else {
                newIndex = (i - 1);
            }
            break;
        }
    }
    if(newIndex != -1) {
        mainTabs->setCurrentIndex(newIndex);
    } else {
        qWarning() << "Unhandled key combination accessed changeFocusedTab: "
                   << shortcut;
        abort();
    }
}

/*
 * Creates a dialog to alert of unsaved changes.
 * Used when closing a tab or quitting the application.
 *
 * Returns(int): function error value
 */
int MainWindow::alertUnsavedChanges(QString closeSource) {
    QMessageBox alertBox(this);

    //configure alertBox settings
    alertBox.setWindowTitle("Unsaved Changes");

    //-1 to assume error/unsaved
    int funcError = -1;
    if("tabClose" == closeSource) {
        alertBox.setText("The contents of this tab have been modified.");
        alertBox.setInformativeText("Do you want to save your changes?");
        alertBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel | QMessageBox::Discard);
        alertBox.setDefaultButton(QMessageBox::Save);
    } else if("appClose" == closeSource) {
        alertBox.setText("The contents of some tabs have been changed.");
        alertBox.setInformativeText("Do you want to save your changes?");
        alertBox.setStandardButtons(QMessageBox::SaveAll | QMessageBox::Cancel | QMessageBox::Discard);
        alertBox.setDefaultButton(QMessageBox::SaveAll);
    } else {
        //shouldn't ever reach here
        qCritical() << "Something called alertUnsavedChanges unhandled: " << closeSource;
        abort();
    }

    int clicked = alertBox.exec();

    //take action based on button clicked
    switch(clicked) {
        case QMessageBox::Cancel:
            break;
        case QMessageBox::Discard:
            //discard all tabs if quitting entire application
            if("appClose" == closeSource) {
                int tabCount = mainTabs->count();
                for(int i = tabCount - 1; i >= 0; --i) {
                    fullyCloseTab(i);
                }
                funcError = 1;
            } else {
                funcError = 0;
            }
            break;
        case QMessageBox::Save:
            funcError = saveFile();
            break;
        case QMessageBox::SaveAll:
            for(int tabIndex = 0; tabIndex < mainTabs->count(); ++tabIndex) {
                funcError = saveFile(tabIndex);
                if(funcError != 0) {
                    break;
                }
            }
            break;
        default:
            //shouldn't ever be reached
            qCritical() << "An unhandled button was clicked in alertUnsavedChanges: " << clicked;
            abort();
    }

    return funcError;
}

/*
 * Dialog for 'About' information
 */
void MainWindow::about() {
    QMessageBox::about(this, tr("About fncEditor"),
                       tr("<h3>fncEditor</h3>" \
                          "<p>©2017, Alden Davidson</p>" \
                          "<p>adavidson@protonmail.ch</p>"));
}
