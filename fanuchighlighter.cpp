/* Copyright 2017, Alden Davidson (adavidson@protonmail.ch) */

/* This file is part of fncEditor.
 *
 * fncEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fncEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fncEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Inspiration, some color choices, and some keyword groupings taken from vim-tp
 * https://github.com/onerobotics/vim-tp
 *
 * The MIT License (MIT)
 * Copyright © 2014 ONE Robotics Company LLC
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include "fanuchighlighter.h"

fanucHighlighter::fanucHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent) {
    HighlightingRule rule;

    tpConditional.setForeground(Qt::darkYellow);
    tpConditional.setFontWeight(QFont::Bold);
    QStringList tpConditionalPatterns;
    tpConditionalPatterns << "\\bIF\\b" << "\\bTHEN\\b" << "\\bENDIF\\b"
                          << "\\bELSE\\b" << "\\bSELECT\\b" << "\\bFOR\\b"
                          << "\\bTO\\b" << "\\bDOWNTO\\b" << "\\bENDFOR\\b";
    foreach(const QString& pattern, tpConditionalPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = tpConditional;
        highlightingRules.append(rule);
    }

    tpLabel.setForeground(Qt::darkYellow);
    tpLabel.setFontWeight(QFont::Bold);
    QStringList tpLabelPatterns;
    tpLabelPatterns << "\\bJMP\\b" << "\\bLBL\\b";
    foreach (const QString& pattern, tpLabelPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = tpLabel;
        highlightingRules.append(rule);
    }

    tpComment.setForeground(Qt::darkGray);
    rule.pattern = QRegularExpression("![^\n]*");
    rule.format = tpComment;
    highlightingRules.append(rule);

    tpBoolean.setForeground(Qt::darkCyan);
    QStringList tpBooleanPatterns;
    tpBooleanPatterns << "\\bON\\b" << "\\bOFF\\b" << "\\bTRUE\\b"
                      << "\\bFALSE\\b" << "\\bENABLE\\b" << "\\bDISABLE\\b";
    foreach (const QString& pattern, tpBooleanPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = tpBoolean;
        highlightingRules.append(rule);
    }

    tpData.setForeground(Qt::darkMagenta);
    tpData.setFontWeight(QFont::Bold);
    QStringList tpDataPatterns;
    tpDataPatterns << "\\bAI\\b" << "\\bAO\\b" << "\\bDI\\b"
                   << "\\bDO\\b" << "\\bF\\b" << "\\bGI\\b"
                   << "\\bGO\\b" << "\\bP\\b" << "\\bPR\\b"
                   << "\\bAR\\b" << "\\bR\\b" << "\\bRI\\b"
                   << "\\bRO\\b" << "\\bRSR\\b" << "\\bSI\\b"
                   << "\\bSO\\b" << "\\bSR\\b" << "\\bUI\\b"
                   << "\\bUI\\b" << "\\bUO\\b" << "\\bVR\\b"
                   << "\\bRESUME_PROG\\b" << "\\bTIMER\\b"
                   << "\\bTIMER_OVERFLOW\\b" << "\\bUALM\\b" << "\\bUFRAME\\b"
                   << "\\bUTOOL\\b" << "\\bMESSAGE\\b"
                   << "\\bJOINT_MAX_SPEED\\b" << "\\bPAYLOAD\\b"
                   << "\\bFOUND_POS\\b" << "\\bMES\\b" << "\\bDIAG_REC\\b"
                   << "\\bDIAG_REC_SEC\\b" << "\\bPL\\b";
    foreach (const QString& pattern, tpDataPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = tpData;
        highlightingRules.append(rule);
    }

    tpUnit.setForeground(QBrush(QColor("#E9C2A6")));
    QStringList tpUnitPatterns;
    tpUnitPatterns << "\\bsec\\b" << "\\bmsec\\b" << "\\b%\\b"
                   << "\\bmm\\b" << "\\bdeg\\b" << "\\bmm/sec\\b"
                   << "\\bcm/min\\b" << "\\bdeg/sec\\b" << "\\bin/min\\b";
    foreach (const QString& pattern, tpUnitPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = tpUnit;
        highlightingRules.append(rule);
    }

    tpNumber.setForeground(Qt::darkBlue);
    tpNumber.setFontWeight(QFont::Bold);
    //exclude numbers touching letters
    //include digits with 0-1 minus signs, 0-1 decimals, and any number of digits following a decimal
    rule.pattern = QRegularExpression("-?\\d+\\.?\\d*");
    rule.format = tpNumber;
    highlightingRules.append(rule);
}

void fanucHighlighter::highlightBlock(const QString &text) {
    foreach (const HighlightingRule& rule, highlightingRules) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while(matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
    setCurrentBlockState(0);
}
