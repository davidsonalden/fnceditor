/* Copyright 2017, Alden Davidson (adavidson@protonmail.ch) */

/* This file is part of fncEditor.
 *
 * fncEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * fncEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with fncEditor.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "fanuchighlighter.h"
#include "codeeditor.h"

#include <QMainWindow>
#include <QTabWidget>
#include <QAction>
#include <QMenuBar>
#include <QFileInfo>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:

public slots:
    //file
    void newTab();
    void closeTab();
    void openFile();
    void quitMain();
    int saveFile(const int& index = mainTabs->currentIndex());
    int saveFileAs(const int& index = mainTabs->currentIndex());

    //view
    void toggleCenterOnScoll();
    void toggleWordWrap();

    //help
    void about();

private slots:
    void tabContentsModified();
    void tabOrderModified(int from, int to);
    void changeFocusedTab();

private:
    //initialization functions
    void initTabWidget();
    void initMenuBar();
    void initFileActions();
    void initViewMenu();
    void initHelpMenu();
    void initTabShortcuts();
    void initTextEditor(CodeEditor* editor);

    //dialogs
    int alertUnsavedChanges(QString closeSource);

    //helper functions
    void fullyCloseTab(int index);

    static QTabWidget* mainTabs;
    QVector<QFileInfo> openFiles;
    fanucHighlighter* highlighter;
};

#endif // MAINWINDOW_H
